import { Component, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  images:any[] = [];
  searchkey:string ="";
  public searchTerm : string = "";

  clickeventsubcription:Subscription;

  isShownv :boolean = true;

  vijaya_on(){
    this.isShownv= ! this.isShownv;
  }

  // public filterCategory : any;
  

  constructor(private imageService:ImageservicesService) { 

    this.imageService.search.subscribe((val:any)=>{
      this.searchkey = val;
    })


    this.clickeventsubcription= this.imageService.getclickevent().subscribe(()=>{
      this.vijaya_on()
    })

  }

  ngOnInit(): void {

    this.images = this.imageService.getAll()
  }


  // this.imageService.search.subscribe((val:any)=>{
  //   this.searchkey = val;
  // })

  search(event:any){
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.imageService.search.next(this.searchTerm);
  }

}
