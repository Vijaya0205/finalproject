import { Injectable } from '@angular/core'; 
import { BehaviorSubject,Observable,Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ImageservicesService {

  public search = new BehaviorSubject<string>("");
  getProducts: any;
  

  private subject=new Subject<void>();

  sendclickevent(){
    this.subject.next();
  }

getclickevent():Observable<any> {

    return this.subject.asObservable();
}

  constructor() { }

  getAll():any[]{
    return  [
      {"id": 1,"title": "amazon athena", category:"Database","image":'/assets/pictures/Amazon Athena.jpg.png', "url":1},
       {"id": 2,"title": "azure_database", category:"Database","image": '/assets/pictures/azure_database.png',  "url":2},
          {"id": 3,"title": "Presto", category:"Database","image": '/assets/pictures/Presto.png' ,"url": 3},
          {"id": 4,"title": "MongoDb", category:"Database","image": '/assets/pictures/MongoDB.png', "url":4},
          {"id": 5,"title": "Microsoft Sql", category:"Database","image": '/assets/pictures/Microsoft SQL Server.png', "url":5},
          {"id": 6,"title": "PostgreSql", category:"Database","image": '/assets/pictures/PostgreSQL.png', "url":6},
          { "id": 7, "title": "mysql", category:"Crm", "image": '/assets/pictures/MySQL.png', "url":7},
          {"id": 8, "title": "Databricks",  category:"Crm", "image": '/assets/pictures/Databricks.png', "url":8},
          { "id": 9, "title": "Amazon s3", category:"Crm", "image": '/assets/pictures/Amazon S3.png', "url":9},
          { "id": 10, "title": "Amazon Redshft",category:"Analytics", "image": '/assets/pictures/Amazon Redshift.png', "url":10},
          { "id": 11, "title": "microsoft sql", category:"Analytics", "image": '/assets/pictures/Microsoft SQL Server.png',"url":11},
          { "id": 12, "title": "Apache Phoenix", category:"Cloudstorage", "image": '/assets/pictures/Apache Phoenix.png', "url":12},
          { "id": 13, "title": "Apache hive", category:"Cloudstorage", "image": '/assets/pictures/Apache Hive.png', "url":13},
          { "id": 14, "title": "Apache spark", category:"E-commerce", "image": '/assets/pictures/Apache Spark.png', "url":14},
          { "id": 15, "title": "Salesforce", category:"E-commerce", "image": '/assets/pictures/Salesforce.png', "url":15},
          { "id": 16, "title": "Dropbox", category:"Database", "image": '/assets/pictures/Dropbox.png', "url":16},
          { "id": 17, "title": "Apache drill", category:"Cloudstorage", "image": '/assets/pictures/Apache Drill.png', "url":17},
          { "id": 18, "title": "Gooogle sheet", category:"Crm", "image": '/assets/pictures/Google Sheets.png', "url":18},
          { "id": 19, "title": "Google drive", category:"E-commerce", "image": '/assets/pictures/Google Drive.png',"url":19},
          { "id": 20, "title": "Google bigquery", category:"Database","image": '/assets/pictures/Google BigQuery.png',"url":20},
    ]
  }

  getAlldatabases():any[]{
    return  [  { "id": 1, "title": "amazon athena", category:"Database", "image":'/assets/pictures/Amazon Athena.jpg.png',"url":21},
    {"id": 2, "title": "Databricks",  category:"Crm", "image": '/assets/pictures/Databricks.png',"url":22},
    { "id": 3, "title": "Amazon s3", category:"Crm", "image": '/assets/pictures/Amazon S3.png',"url":23},
    { "id": 4, "title": "Amazon Redshft",category:"Analytics", "image": '/assets/pictures/Amazon Redshift.png',"url":24},
    { "id": 5, "title": "microsoft sql", category:"Analytics", "image": '/assets/pictures/Microsoft SQL Server.png',"url":25},
    { "id": 6, "title": "Apache Phoenix", category:"Cloudstorage", "image": '/assets/pictures/Apache Phoenix.png',"url":26},
    { "id": 7, "title": "Apache hive", category:"Cloudstorage", "image": '/assets/pictures/Apache Hive.png',"url":27},
    { "id": 8, "title": "Apache spark", category:"E-commerce", "image": '/assets/pictures/Apache Spark.png',"url":28},
    { "id": 9, "title": "Salesforce", category:"E-commerce", "image": '/assets/pictures/Salesforce.png',"url":29},
    { "id": 10, "title": "Dropbox", category:"Database", "image": '/assets/pictures/Dropbox.png',"url":30},
    { "id": 11, "title": "Apache drill", category:"Cloudstorage", "image": '/assets/pictures/Apache Drill.png',"url":31},
    { "id": 12, "title": "mysql", category:"Crm", "image": '/assets/pictures/MySQL.png',"url":32},

  ]
}

mysqlvariants():any[] {
  return [ {"id": 1, "title": "Mysql", category:"Crm", "image": '/assets/pictures/MySQL.png',"url":100 },
  { "id": 2, "title": "Microsoft sql", category:"Analytics", "image": '/assets/pictures/Microsoft SQL Server.png',"url":200},
  { "id": 3, "title": "Amazon s3", category:"Crm", "image": '/assets/pictures/Amazon S3.png',"url":300},
  { "id": 4, "title": "Amazon athena", category:"Database", "image":'/assets/pictures/Amazon Athena.jpg.png',"url":400},
  { "id": 9, "title": "Salesforce", category:"E-commerce", "image": '/assets/pictures/Salesforce.png',"url":500},
  { "id": 8, "title": "Apache spark", category:"E-commerce", "image": '/assets/pictures/Apache Spark.png',"url":28}
  ]

  

}



getUsers(url: string){
  return this.getAll()
}


get_product_name_from(product_id: number){
  if(product_id == 100){
    return {"id": 1, "title": "mysql", category:"Crm", "image": '/assets/pictures/MySQL.png',"url":100}
  }else if(product_id == 200){
    return {"id": 1, "title": "microsoft sql", category:"Crm", "image": '/assets/pictures/MySQL.png',"url":100 }
  }else if(product_id == 300){
    return {"id": 1, "title": "amazon s3", category:"Crm", "image": '/assets/pictures/MySQL.png',"url":100 }
  }else if(product_id == 400){
    return {"id": 1, "title": "amazon athena", category:"Crm", "image": '/assets/pictures/MySQL.png',"url":100 }
  }
  else if(product_id == 400) {
    return  { "id": 4, "title": "amazon athena", category:"Database", "image":'/assets/pictures/Amazon Athena.jpg.png',"url":400}
    
  }else {
    return { "id": 8, "title": "Apache spark", category:"E-commerce", "image": '/assets/pictures/Apache Spark.png',"url":28}
  }


  
}

  isShownv :boolean = true;

  vijaya_on(){
    this.isShownv= ! this.isShownv;
  }


}
