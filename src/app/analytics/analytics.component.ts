import { Component, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';


@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  images:any[] = [];
  searchkey:string ="";
  public searchTerm : string = "";

  constructor(private imageService:ImageservicesService) { 

    this.imageService.search.subscribe((val:any)=>{
      this.searchkey = val;
    })
  }

  ngOnInit(): void {

    this.images = this.imageService.getAll()
  }


  search(event:any){
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.imageService.search.next(this.searchTerm);
  }


  isShownv :boolean = true;

  clickme(){
    // this.isShownv= ! this.isShownv;
    this.imageService.sendclickevent();

  }


}
