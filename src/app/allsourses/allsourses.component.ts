import { Routes } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-allsourses',
  templateUrl: './allsourses.component.html',
  styleUrls: ['./allsourses.component.css']

})
export class AllsoursesComponent implements OnInit {
  Routes: any;
  user: any;


  images:any[] = [];
  searchkey:string ="";
  public searchTerm : string = "";

  constructor(private imageService:ImageservicesService,
              private route: ActivatedRoute) {

    this.imageService.search.subscribe((val:any)=>{
      this.searchkey = val;
    })


  
   }

  ngOnInit(): void {
    this.images = this.imageService.getAll()


    //url method
    // let url =this.route.snapshot.params['url'];
    // this.imageService.getUsers(url).subscribe((u:any)=> {
       
    //   this.user = u;
    // });
  }

  search(event:any){
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.imageService.search.next(this.searchTerm);
  }




  isShownv :boolean = true;

  clickme(){
    // this.isShownv= ! this.isShownv;
    this.imageService.sendclickevent();

  }



}
