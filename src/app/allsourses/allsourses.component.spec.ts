import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllsoursesComponent } from './allsourses.component';

describe('AllsoursesComponent', () => {
  let component: AllsoursesComponent;
  let fixture: ComponentFixture<AllsoursesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllsoursesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllsoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
