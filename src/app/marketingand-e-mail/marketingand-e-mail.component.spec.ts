import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingandEMailComponent } from './marketingand-e-mail.component';

describe('MarketingandEMailComponent', () => {
  let component: MarketingandEMailComponent;
  let fixture: ComponentFixture<MarketingandEMailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketingandEMailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MarketingandEMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
