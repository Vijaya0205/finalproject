

import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ScrollingModule} from '@angular/cdk/scrolling';


const designComponents = [
                          MatButtonModule,
                          MatToolbarModule,
                          MatIconModule,
                          MatSidenavModule,
                          MatGridListModule,
                          MatFormFieldModule,
                          MatListModule,
                          MatCardModule,
                          MatTooltipModule,
                          ScrollingModule,
];


@NgModule({
  
  imports: [designComponents,MatSidenavModule],
  exports: [designComponents,]
})
export class DesignModule { }
