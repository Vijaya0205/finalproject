import { Component, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';

@Component({
  selector: 'app-databases',
  templateUrl: './databases.component.html',
  styleUrls: ['./databases.component.css']
})
export class DatabasesComponent implements OnInit {


  images:any[] = [];
  searchkey:string ="";
  public searchTerm : string = "";

  constructor(private imageService:ImageservicesService) {

    this.imageService.search.subscribe((val:any)=>{
      this.searchkey = val;
      console.log(val);
    })

  }
  ngOnInit(): void {
    this.images = this.imageService.getAlldatabases()
  }

  search(event:any){
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.imageService.search.next(this.searchTerm);
  }


  isShownv :boolean = true;
  clickme(){
    // this.isShownv= ! this.isShownv;
    this.imageService.sendclickevent();

  }


}
