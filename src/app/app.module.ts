import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DesignModule } from './design/design.module';
import { AllsoursesComponent } from './allsourses/allsourses.component';
import { DatabasesComponent } from './databases/databases.component';
import { CrmComponent } from './crm/crm.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { CloudstorageComponent } from './cloudstorage/cloudstorage.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { MarketingandEMailComponent } from './marketingand-e-mail/marketingand-e-mail.component'; 
import { myRoutings } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { FilterPipe } from './filter.pipe';
import { MysqlvariantsComponent } from './mysqlvariants/mysqlvariants.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    AllsoursesComponent,
    DatabasesComponent,
    CrmComponent,
    ECommerceComponent,
    CloudstorageComponent,
    AnalyticsComponent,
    MarketingandEMailComponent,
    myRoutings,
    HeaderComponent,
    FilterPipe,
    MysqlvariantsComponent,
    ProductdetailsComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    DesignModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
