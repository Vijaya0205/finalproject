import { Component, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.css']
})
export class ProductdetailsComponent implements OnInit {
  data:any= {};
  des1:any={};


  constructor(private imageService:ImageservicesService,private route:ActivatedRoute) { }

  ngOnInit(): void {

    // we have to call product details service from here
    let product_id = this.route.snapshot.params['url'];
    // console.log(this.route);
    
    this.data = this.imageService.get_product_name_from(product_id);



  }

}
