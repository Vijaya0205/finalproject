import { TestBed } from '@angular/core/testing';

import { ImageservicesService } from './imageservices.service';

describe('ImageservicesService', () => {
  let service: ImageservicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageservicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
