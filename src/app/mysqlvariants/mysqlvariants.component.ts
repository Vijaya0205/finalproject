import { Component, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-mysqlvariants',
  templateUrl: './mysqlvariants.component.html',
  styleUrls: ['./mysqlvariants.component.css']
})
export class MysqlvariantsComponent implements OnInit {


  images:any[] = [];
  // searchkey:string ="";
  // public searchTerm : string = "";

  // display=true;
  // toggle()
  // {
  //   this.display=!this.display;
  // }

  constructor(private imageService:ImageservicesService,
             private route:ActivatedRoute) {

    // this.imageService.search.subscribe((val:any)=>{
    //   this.searchkey = val;
    // })

  }
  ngOnInit(): void {
    // this.images = this.imageService.getAlldatabases()
    this.images = this.imageService.mysqlvariants();

    // let url = this.route.snapshot.params['url'];
  }

  // search(event:any){
  //   this.searchTerm = (event.target as HTMLInputElement).value;
  //   this.imageService.search.next(this.searchTerm);
  // }


  isShownv :boolean = true;

  clickme(){
    // this.isShownv= ! this.isShownv;
    this.imageService.sendclickevent();

  }







}
