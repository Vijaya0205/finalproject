import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MysqlvariantsComponent } from './mysqlvariants.component';

describe('MysqlvariantsComponent', () => {
  let component: MysqlvariantsComponent;
  let fixture: ComponentFixture<MysqlvariantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MysqlvariantsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MysqlvariantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
