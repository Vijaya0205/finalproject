import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { MarketingandEMailComponent } from './marketingand-e-mail/marketingand-e-mail.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { CloudstorageComponent } from './cloudstorage/cloudstorage.component';
import { CrmComponent } from './crm/crm.component';
import { DatabasesComponent } from './databases/databases.component';
import { AllsoursesComponent } from './allsourses/allsourses.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { MysqlvariantsComponent } from './mysqlvariants/mysqlvariants.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';


const routes: Routes = [
  {path:'allsourses', component: AllsoursesComponent},
   
  // { path:'mysqlvariants', component:MysqlvariantsComponent},

  { path:'mysqlvariants/:url', component:MysqlvariantsComponent},

  {path:'databases', component:DatabasesComponent},
  {path: 'crm', component: CrmComponent},
  {path: 'e-commerce', component: ECommerceComponent},
  {path: 'cloudstorage', component:CloudstorageComponent}, 
  {path: 'analytics', component:AnalyticsComponent},
  {path: 'marketingandemail', component:MarketingandEMailComponent},
  { path: 'header', component:HeaderComponent},
  // { path:'mysqlvariants', component:MysqlvariantsComponent},
  // { path: 'productdetails', component:ProductdetailsComponent},
  { path: 'productdetails/:url', component:ProductdetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
      
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const myRoutings = [
                          ECommerceComponent,
                          AllsoursesComponent,
                          DatabasesComponent,
                          CrmComponent,
                          CloudstorageComponent,
                          AnalyticsComponent,
                          MarketingandEMailComponent,
                          MysqlvariantsComponent,
                          ProductdetailsComponent

]
