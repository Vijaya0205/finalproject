import { Component, OnInit } from '@angular/core';
import { ImageservicesService } from '../imageservices.service';

@Component({
  selector: 'app-e-commerce',
  templateUrl: './e-commerce.component.html',
  styleUrls: ['./e-commerce.component.css']
})
export class ECommerceComponent implements OnInit {
  images:any[] = [];
  searchkey:string ="";
  public searchTerm : string = "";

  constructor(private imageService:ImageservicesService) { 
    this.imageService.search.subscribe((val:any)=>{
      this.searchkey = val;
    })
  }

  ngOnInit(): void {
    this.images = this.imageService.getAll()
  }
  search(event:any){
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.imageService.search.next(this.searchTerm);
  }

  isShownv :boolean = true;

  clickme(){
    // this.isShownv= ! this.isShownv;
    this.imageService.sendclickevent();

  }


}
